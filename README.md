# Smart XML Analiser
The main purpose of this tool is to find on the web page elements that was a bit modified.

## Usage:
* Download **find_element.py** file.

* Run with command line `python find_element.py` provide **original** and _**_diff_**_ links(required) and options(optional)

* In command line you will see path to searched element with similar attributes that wsa found or **"Nothing found"**
 
## Command line options
* `-p` (or `--percents`) you can provide element similarity percentage. _Default - 50_

* `-a` (or `--attr`) attribute of element on original page. _Default - id_

* `-v` (or `--value`) value of the attribute on original page. _Default - make-everything-ok-button_


## Example
`python find_element.py some-site.com/original some-site.com/diff -a title -v Make-Button -p 40`

In this example original page is **some-site.com/original** page where to find element **some-site.com/diff**.

On original page element that has to be compared could be found by attribute **title=Make-Button**.

And percentage of similarity is **40**, it means that if element has more than 40% of similar attributes, this is suitable element.

## Output example
`{'/html/body/div/div/div[3]/div[1]/div/div[2]/div/a': "found attrs: ['href=#ok', 'title=Make-Button', 'rel=next']"}`

or

`Nothing found`

##Requirements 
python-3.7.4
lxml-4.5.0
