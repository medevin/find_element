import argparse
import requests
from lxml import etree

ORIGINAL = 0
DIFF = 1
TO_PERCENTS = 100


def command_line_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("urls", nargs="+")
    parser.add_argument(
        "--attr", "-a", type=str, default="id", help="element attribute to search for"
    )
    parser.add_argument(
        "--value",
        "-v",
        type=str,
        default="make-everything-ok-button",
        help="attribute value to search for",
    )
    parser.add_argument(
        "--percentage", "-p", type=int, default=50, help="pass percentage"
    )
    args = parser.parse_args()
    return args


def make_requests(urls):
    """ parse html from given links """
    pages_source = []
    for url in urls:
        with requests.Session() as s:
            pages_source.append(etree.fromstring(s.get(url)._content))
    return pages_source


def get_attrs(source, attr, value):
    """ get attributes of the original element """
    element = source.find(f".//*[@{attr}='{value}']")
    element_attrs = element.attrib
    element_text = element.text.strip()

    return element_attrs, element_text


def get_path(source, attr, value):
    """ yields path for found element """
    elements = source.xpath(f".//*[contains(@{attr},'{value}')]")
    for item in elements:
        yield item.getroottree().getpath(item)


def search_corresponding_elements(source, values_to_search):
    """ searches for elements on diff page"""
    path_with_attrs = {}
    for key, value in values_to_search.items():
        for path in get_path(source, key, value):
            if path_with_attrs.get(path):
                path_with_attrs[path].append(f"{key}={value}")
            else:
                path_with_attrs[path] = [f"{key}={value}"]
    return path_with_attrs


def serach_corresponding_text(source, original_text): 
    elements = source.xpath(f".//*[contains(text(),'{original_text}')]")
    pass # TODO: add search by text


def get_diff_element(path_with_attrs, pass_percentage):
    """ count number of similar attributes find per element
        count percentage of similarity (default 50%) """
    original_attrs_count = len(original_attrs)
    found_element = {
        path: f"found attrs: {attrs}"
        for path, attrs in path_with_attrs.items()
        if (len(attrs) / original_attrs_count * TO_PERCENTS) >= pass_percentage
    }
    return found_element or "Nothing found"


args = command_line_parse()
pages_source = make_requests(args.urls)
original_attrs, original_text = get_attrs(pages_source[ORIGINAL], args.attr, args.value)
path_with_attrs = search_corresponding_elements(pages_source[DIFF], original_attrs)
elements_found = get_diff_element(path_with_attrs, args.percentage)

print(elements_found)
